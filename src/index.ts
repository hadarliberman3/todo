import * as todo_func from "./myModule.js";
import {start} from './db/data.js';

 async function init(){    
    await start();
    var myArgs = process.argv.slice(2);
        switch (myArgs[0]) {
            case 'create':
                myArgs.shift();
               await todo_func.create(myArgs);
                break;
            case 'read':
            console.table(await todo_func.read());
                break;
            case 'update':
                await todo_func.update(myArgs[1]);
                break;
            case 'count':
                let counter = await todo_func.count();
                console.log(' number of tasks is: ', counter);
                    break;
            case 'delete_t':
                myArgs.shift();
                await todo_func.delete_task(myArgs);
                break;
            case 'all':
                console.table(await todo_func.all());
                break;
            case 'completed':
                await todo_func.completed();
                break;
            case 'open_tasks':
                await todo_func.open_tasks();
                break;
            case 'remove_complited':
                await todo_func.remove_complited(myArgs[1]);
                break;
            case 'help':
            default:
                console.log(await todo_func.options());
            }
}
init();


import fs from 'fs/promises';
import { getData, save, task } from './db/data.js';
const uid = () => Math.random().toString(36).substring(2);

export async function create(args: string[]): Promise<void> {
    let data = await getData();
    for(let todo of args){
        let task: task = {
            id: uid(),
            name: todo,
            isComleted: false
        }
        data.push(task);
    }
    
    save(data);
}

export async function count(){
    let data = await getData();
    return data.length;

}

export async function read(): Promise<task[]> {
    return getData();
}
export async function update(id: string): Promise<void> {
    let data = await getData();
    for(let task of data){
        if(task.id === id){
            task.isComleted = !(task.isComleted);
        }
    }
    save(data);
}

export async function delete_task(ids: string[]): Promise<void> {
    let data = await getData();
    for(let id of ids){
        data = data.filter((task) => task.id!==id);
    }
    save(data);
}
export async function open_tasks(): Promise<task[]>{
    let data = await getData();
    let open  = data.filter((task) => task.isComleted===false);
    return open;
}
export async function completed():  Promise<task[]>{
    let data = await getData();
    let completed_task  = data.filter((task) => task.isComleted===true);
    return completed_task;
}
export async function all(): Promise<task[]> {
    let data = getData();
    return data;
}
export async function remove_complited(id: string):Promise<task[]>{
    let data = await getData();
     data = await open_tasks();
     await fs.writeFile('./todo.json', JSON.stringify(data, null, 2));
     return data;
}

export async function options(): Promise<any> {
   let data= await fs.readFile('./options.txt', 'utf8');
   //let data = JSON.parse(data_json);
   return data; 
}